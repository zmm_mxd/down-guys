package com.downguys.microservice;

/**
 * @author mingmin.zhou(弄死熊猫)
 * @mail 714387891@qq.com
 * @date 2017/7/18
 */
public interface Start {

    void start(final boolean sslFlag);

    void stop();

}

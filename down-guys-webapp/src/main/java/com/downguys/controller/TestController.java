package com.downguys.controller;

import com.downguys.model.Test;
import com.downguys.service.TestService;
import org.apache.log4j.Logger;
import org.jsondoc.core.annotation.*;
import org.jsondoc.core.pojo.ApiStage;
import org.jsondoc.core.pojo.ApiVisibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author mingmin.zhou(弄死熊猫)
 * @mail 714387891@qq.com
 * @date 2016/11/21
 */
@Api(name = "TestController", description = "Methods for managing test", group = "test", visibility = ApiVisibility.PUBLIC, stage = ApiStage.RC)
@ApiVersion(since = "1.0", until = "2.0")
@Controller
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
    private TestService testService;

    public static final Logger logger = Logger.getLogger(TestController.class);

    @ApiMethod(path = "/{name}",description = "测试mvc",produces = {MediaType.ALL_VALUE})
    @RequestMapping(value = "/{name}")
    @ResponseBody
    public @ApiResponseObject(clazz = Test.class) Test test(@ApiPathParam(description = "The name of the city",name = "name") @PathVariable(name = "name",required = false) String name,
                                                            @ApiQueryParam(description = "queryParam",name = "testName") @RequestParam(name = "testName") String testName ){
        Test test = new Test();
        test.setTestName(testName);
        test.setTestPassword("zmm");
        logger.info(name+"\n");
        logger.info(testName+"\n");

        return test;
    }

    @ApiMethod(path = "/hello",description = "hello")
    @RequestMapping(value = "/hello",method = RequestMethod.POST)
    @ResponseBody
    public Test hello(@RequestBody @ApiBodyObject Test test){

        testService.test("1");
        return test;
    }

    @RequestMapping("/testDoc")
    @ResponseBody
    public void testDoc(HttpServletRequest request, HttpServletResponse response){
        try {
            response.sendRedirect("http://localhost:8080/jsonDoc/jsondoc-ui.html?url=http://localhost:8080/jsondoc");
        } catch (IOException e) {
            logger.error("testDoc error:",e);
        }
    }


}

package com.downguys.controller.user;

import com.downguys.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author mingmin.zhou(弄死熊猫)
 * @mail 714387891@qq.com
 * @date 2017/1/4
 */
@Controller
@RequestMapping("user")
public class UserController extends BaseController{

    @RequestMapping("register")
    @ResponseBody
    public void register(){

    }

    @RequestMapping("login")
    @ResponseBody
    public void login(){
        System.out.println("login----------------------1");
    }


}

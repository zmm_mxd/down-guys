package com.downguys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.downguys.dao.TestMapper;
import com.downguys.model.Test;
import com.downguys.service.TestService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2016/11/30.
 */
@Service
public class TestServiceImpl implements TestService {

    public static final Logger logger = Logger.getLogger(TestServiceImpl.class);
    @Autowired
    private TestMapper testMapper;

    @Override
    public void test(String testStr) {
        Test test = testMapper.selectByPrimaryKey(Long.parseLong(testStr));
        System.out.println("test transaction1 :--------------"+ JSONObject.toJSONString(test));
        logger.info("test transaction2 :--------------"+ JSONObject.toJSONString(test));
    }
}

package com.downguys.tool.thread;

/**
 * @author mingmin.zhou(弄死熊猫)
 * @mail 714387891@qq.com
 * @date 2016/12/30
 */
public class ThreadNames {

    public static final String NS = "dg";

    public static final String THREAD_NAME_PREFIX = NS + "-t";

    public static final String T_EVENT_BUS = NS + "-event";
}

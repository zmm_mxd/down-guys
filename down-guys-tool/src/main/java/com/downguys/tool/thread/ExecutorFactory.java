package com.downguys.tool.thread;

import java.util.concurrent.Executor;

/**
 * @author mingmin.zhou(弄死熊猫)
 * @mail 714387891@qq.com
 * @date 2016/12/30
 */
public interface ExecutorFactory {

    String EVENT_BUS = "eb";

    Executor get(String name);
}

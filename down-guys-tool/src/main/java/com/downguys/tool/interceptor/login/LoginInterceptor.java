package com.downguys.tool.interceptor.login;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.downguys.tool.httptool.HttpUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;

/**
 * Created by mingmin.zhou (弄死熊猫) on 2017/1/21.
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {

    public static final String URL = "";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        ResponseEntity<Object> resp = HttpUtil.syncGetOrPostForJson(URL, "get");
        // TODO 具体解析返回值，先简单解析一下
        Object obj = resp.getBody();

        JSONObject jsonObject = JSONObject.parseObject(JSON.toJSONString(obj));
        boolean isLogin = jsonObject.get("isLogin") == null ? false : Boolean.parseBoolean(jsonObject.get("isLogin")
                .toString());
        return isLogin;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
    }
}

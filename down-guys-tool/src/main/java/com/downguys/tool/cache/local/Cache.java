package com.downguys.tool.cache.local;

import com.downguys.tool.cache.CacheInterface;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mingmin.zhou (Ū����è) on 2017/1/21.
 */
public class Cache implements CacheInterface<String> {

    private LoadingCache<String,String> cache;

    @Override
    public void init(Map<String,String> initMap) {
        cache = CacheBuilder.newBuilder().build(new CacheLoader<String, String>() {
            @Override
            public String load(String key) throws Exception {
                return null;
            }
        });
        cache.putAll(initMap);
    }

    @Override
    public Map<String, String> getCache() {
        return cache.asMap();
    }

    @Override
    public String getCacheValue(String key) {
        return cache.asMap().get(key);
    }

    @Override
    public void put(Map<String, String> map) {

    }

}

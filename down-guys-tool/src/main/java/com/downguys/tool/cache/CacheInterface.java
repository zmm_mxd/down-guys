package com.downguys.tool.cache;

import java.util.Map;

/**
 * Created by mingmin.zhou (Ū����è) on 2017/1/21.
 */
public interface CacheInterface<T> {

    void init(Map<String,T> map);

    Map<String,T> getCache();

    T getCacheValue(String key);

    void put(Map<String,T> map);

}

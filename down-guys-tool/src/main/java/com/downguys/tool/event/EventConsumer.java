package com.downguys.tool.event;

/**
 * @author mingmin.zhou(弄死熊猫)
 * @mail 714387891@qq.com
 * @date 2016/12/30
 */
public class EventConsumer {

    public EventConsumer() {
        EventBus.I.register(this);
    }

}

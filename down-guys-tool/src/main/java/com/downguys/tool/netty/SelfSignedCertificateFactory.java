package com.downguys.tool.netty;

import io.netty.handler.ssl.util.SelfSignedCertificate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.cert.CertificateException;

/**
 * SelfSignedCertificate工厂类，目的是预先创建证书及密钥等内容。
 * @author tony04.liu
 *
 */
public final class SelfSignedCertificateFactory {
	private static final Logger logger = LoggerFactory.getLogger(SelfSignedCertificateFactory.class);
	private static SelfSignedCertificate instance;
	
	public static SelfSignedCertificate getInstance() throws Exception{
		if(instance == null){ // NOSONAR
			synchronized(SelfSignedCertificate.class){
				if(instance == null){
					try {
						long start = System.currentTimeMillis();						//统计耗时
						instance = new SelfSignedCertificate();
						long end = System.currentTimeMillis();							//统计耗时
						logger.info("instantiate SelfSignedCertificate consume："+(end-start));	//统计耗时
					} catch (CertificateException e) {
						logger.error(e.getMessage(), e);
						throw new Exception("instantiate SelfSignedCertificate fail.", e);
					}
				}
			}
		}
		
		return instance;
	}
}

package com.downguys.tool.netty;

import io.netty.handler.ssl.OpenSsl;
import io.netty.util.internal.InternalThreadLocalMap;
import io.netty.util.internal.LongCounter;
import io.netty.util.internal.ThreadLocalRandom;

import java.util.concurrent.ConcurrentMap;

public class PlatformDependent {

	public static boolean isOpenSSLAvaible() {
		System.out.println("OpenSSL: " //NOSONAR
				+ (OpenSsl.isAvailable() ? "yes (" + OpenSsl.versionString() + ", " + OpenSsl.version() + ')'
						: "no (" + filterCause(OpenSsl.unavailabilityCause()) + ')'));

		return OpenSsl.isAvailable();
	}

	public static <K, V> ConcurrentMap<K, V> newConcurrentHashMap() {
		return io.netty.util.internal.PlatformDependent.newConcurrentHashMap();
	}

	public static <K, V> ConcurrentMap<K, V> newConcurrentHashMap(int initialCapacity) {
		return io.netty.util.internal.PlatformDependent.newConcurrentHashMap(initialCapacity);
	}

	public static <K, V> ConcurrentMap<K, V> newConcurrentHashMap(int initialCapacity, float loadFactor,
			int concurrencyLevel) {
		return io.netty.util.internal.PlatformDependent.newConcurrentHashMap(initialCapacity, loadFactor,
				concurrencyLevel);
	}

	public static LongCounter newLongCounter() {
		return io.netty.util.internal.PlatformDependent.newLongCounter();
	}

	public static StringBuilder stringBuilder() {
		return InternalThreadLocalMap.get().stringBuilder();
	}

	public static ThreadLocalRandom threadLocalRandom() {
		return InternalThreadLocalMap.get().random();
	}

	

	private static Throwable filterCause(Throwable cause) {
		if (cause instanceof ExceptionInInitializerError) {
			return cause.getCause();
		}

		return cause;
	}

}

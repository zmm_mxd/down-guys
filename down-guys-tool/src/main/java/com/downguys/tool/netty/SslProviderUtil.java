package com.downguys.tool.netty;

import io.netty.handler.ssl.SslProvider;

/**
 * Property的工具类
 */
public class SslProviderUtil {
	public static final String KEY_SSL_PROVIDER = "osp.sslprovider.openssl";
	public static final String KEY_HTTPS_GCM = "osp.https.gcm";

	/** 获取环境配置中定义的sslprovider */
	public static SslProvider getSslProvider() {
		if (Boolean.parseBoolean(System.getProperty(KEY_SSL_PROVIDER, "true"))
				&& PlatformDependent.isOpenSSLAvaible()) {
			return SslProvider.OPENSSL;
		}

		// 默认是JDK的实现方式
		return SslProvider.JDK;
	}

	public static void setSslProvider(SslProvider provider) {
		if (SslProvider.OPENSSL == provider) {
			System.setProperty(KEY_SSL_PROVIDER, "true");
		} else {
			System.setProperty(KEY_SSL_PROVIDER, "false");
		}
	}

	public static boolean isHttpsUseGcm() {
		return Boolean.getBoolean(KEY_HTTPS_GCM);
	}
}
